﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SwitchCamera : MonoBehaviour {

	public List<GameObject> players;
	

	void Start () {
		// Deactivate all but the first camera.
		for(int i = 0; i < players.Count; i++) {
			if (i > 0) {
				players[i].SetActive(false);
			}
		}	
	}
	
	void Update () {
		if (Input.GetKeyDown ("c")) {
			// Activate the next camera.
			for(int i = 0; i < players.Count; i++) {
				if (players[i].activeSelf) {
					players[i].SetActive(false);
					players[(i + 1) % (players.Count)].SetActive(true);
					break;
				}
			}
		}
	}
}
