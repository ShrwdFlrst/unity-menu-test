﻿namespace Maido.Utils {
	using UnityEngine;
	using System.Collections;

	public class FloatMove : MonoBehaviour {

		public const string WaypointTag = "Waypoint_Float";

		public int minHoverTime = 40;
		public float moveSpeed = 5f;
		public float maxDistance = 1000;

		private Transform playerTransform;
		private Transform waypointTransform;
		private RaycastHit hit;
		private int hoverTime;
		private bool moving = false;

		private Vector3 fwd;

		void Start () {
			// What should we move?
			playerTransform = transform.parent.transform;
		}
		
		void Update () {
			fwd = transform.TransformDirection(Vector3.forward);

			if (Physics.Raycast (transform.position, fwd, out hit, maxDistance)) {
				UpdateHoverTime ();
			}
			CheckShouldMove ();
			MoveToWaypoint ();
		}

		void UpdateHoverTime() {
			if (hit.collider.gameObject.tag == FloatMove.WaypointTag && !moving) {
				hoverTime++;
			} else {
				hoverTime = 0;
			}
		}

		void CheckShouldMove() {
			if (hoverTime >= minHoverTime) {
				hoverTime = 0;
				moving = true;
				waypointTransform = hit.transform;
			}
		}
		
		void MoveToWaypoint() {
			if (waypointTransform != null && moving) {
				float dist = Vector3.Distance(playerTransform.position, waypointTransform.position);
				dist = Mathf.Max(1, dist);

				playerTransform.position = Vector3.MoveTowards(
					playerTransform.position, 
					waypointTransform.position, 
					moveSpeed * Time.deltaTime * dist
					);

				// Stop moving if at destination.
				if (playerTransform.position == waypointTransform.position) {
					moving = false;
					waypointTransform = null;
				}
			}
			
		}

	}
}
