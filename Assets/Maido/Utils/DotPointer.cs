namespace Maido.Utils {
	using UnityEngine;
	using System.Collections;

	[AddComponentMenu("DotPointer")]
	public class DotPointer : MonoBehaviour {

		public GameObject dot;
		public float maxDistance = 1000;
		public float minDistance = 8;

		private Vector3 fwd;
		private RaycastHit hit;
		private Animator anim;
		private NavMeshAgent agent;



		void Start () {
			Cursor.lockState = CursorLockMode.Locked;
			Cursor.visible = false;
		}
		
		void Update () {
			DrawDot ();

		}

		void DrawDot () {
			fwd = transform.TransformDirection(Vector3.forward);
			
			if (dot != null && Physics.Raycast (transform.position, fwd, out hit, maxDistance)) {
				dot.transform.position = hit.point;
				dot.transform.rotation = Quaternion.FromToRotation(transform.up, hit.normal) * transform.rotation;
				anim = hit.collider.gameObject.GetComponent<Animator>();
				if (anim != null) {
					anim.Play("Hover");
				}
			} else {
				Ray r = new Ray(transform.position, fwd);
				dot.transform.position = r.GetPoint(minDistance);
				dot.transform.rotation = Quaternion.FromToRotation(transform.up, dot.transform.position) * transform.rotation;
			}
		}

		public RaycastHit GetHit() {
			return hit;
		}
	}
}
